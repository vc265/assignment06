//Vicky Chou

window.onload = function () {
    // Image Display
    var index = 0;
    var images = ["images/Joy.jpg", "images/Anger.jpg", "images/Disgust.jpg", "images/Fear.jpg"];
    var images_container_div = document.getElementById("images_container");
    var display_image = document.createElement("img");
    
    display_image.setAttribute("src", images[index]);
    images_container_div.appendChild(display_image);
    
    function previous_image() {
        if (index > 0) {
            index -= 1;
        }
        else {
            index = images.length - 1;
        }
        display_image.setAttribute("src", images[index]);
        images_container_div.appendChild(display_image);
    }
    
    function next_image() {
        if (index < images.length - 1) {
            index += 1;
        }
        else {
            index = 0;
        }
        display_image.setAttribute("src", images[index]);
        images_container_div.appendChild(display_image);
    }
    document.getElementById("previous").addEventListener('click', function() {
        previous_image();
    });
    
    document.getElementById("next").addEventListener('click', function() {
        next_image();
    });
    
    // Date Display
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var new_date = new Date();
    var display_year = new_date.getFullYear();
    var display_month = months[new_date.getMonth()];
        // getMonth returns an index from 0-11
    var display_date = new_date.getDate();
    var display_text = document.createTextNode(display_date + ' ' + display_month + ' ' + display_year);
    var footer = document.getElementsByTagName("footer")[0];
    
    footer.appendChild(display_text);
}

